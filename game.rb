# --------------
# Game is where the game is executed
# 1. cd into the ruby directory in the command line
# 2. Type `ruby game.rb` in the command line
# --------------

require_relative "logic/game_controller"

game = GameController.new
game.run
