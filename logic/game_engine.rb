# --------------
# GameEngine
# Make sure the game adheres to rules
# --------------
class GameEngine
  attr_reader :board

  def initialize(board)
    @grid = board.current_board[1]
    @dimension = board.current_board[0]
  end

  def winner?
    winner_symbol != nil
  end

  def draw?
    full? && !winner?
  end

  def full?
    @grid.count(0).zero?
  end

  def game_over?
    winner? || draw?
  end

  def winner_symbol
    winner = nil
    lines.each do |line|
      winner = line[0] if all_same_symbols?(line)
    end
    winner
  end

  def all_same_symbols?(line)
    all_same = false
    all_same = true if line.uniq.length == 1 && (line & [1, 2]).any?
    all_same
  end

  def lines
    [rows, columns, diagonals].flatten(1)
  end

  def rows
    @grid.each_slice(@dimension).to_a
  end

  def columns
    @grid.each_slice(@dimension).to_a.transpose
  end

  def diagonals
    first = [], second = []
    (0...@dimension).each do |i|
      first[i] = @grid[(@dimension * i) + i]
    end
    offset = @dimension - 1
    (0...@dimension).each do |i|
      second[i] = @grid[offset]
      offset += @dimension - 1
    end
    [first, second]
  end
end
