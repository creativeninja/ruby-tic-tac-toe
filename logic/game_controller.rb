require_relative "game_engine"
# --------------
# Game Controller
# Controls the game
# --------------
class GameController
  attr_reader :board, :players

  def initialize(board, players)
    @board = board
    @players = players
    @game_engine = GameEngine.new(board)
  end

  def whos_turn?
    p1 = board.current_board[1].count(1)
    p2 = board.current_board[1].count(2)
    if p1 > p2
      2
    elsif p2 > p1
      1
    elsif p2 == p1
      1
    end
  end

  def end_result
    if @game_engine.game_over?
      if @game_engine.draw? == true
        "draw"
      elsif @game_engine.winner_symbol != nil?
        @game_engine.winner_symbol
      end
    end
  end

  def play_game(positions = nil)
    until @game_engine.game_over?
      positions.map do |position|
        players[whos_turn?].make_move(board, position)
      end
    end
    "end"
  end

  def run(positions = nil)
    loop do
      play_game(positions)
      break
    end
    { "result" => end_result, "board" => board.current_board[1] }
  rescue Interrupt
    "interrupt"
  end
end
