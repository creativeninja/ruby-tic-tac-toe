# --------------
# Create computer player
# --------------
class PlayerComputer
  attr_reader :player, :difficulty

  def initialize(player, difficulty)
    @player = player
    @difficulty = difficulty
  end

  def make_move(board, position = nil)
    move = { position: position, player: player }
    update_board = board.place(move)
    { "board" => update_board, "position" => position, "player" => player }
  end
end
