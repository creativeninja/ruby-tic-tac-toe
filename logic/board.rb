# --------------
# Board
# Responsible for keeping the state of the board
# --------------
class Board
  attr_reader :dimension, :grid
  def initialize(dimension = 3, grid = [])
    @dimension = dimension
    grid = board_array if grid.empty?
    @grid = grid
  end

  def place(position:, player:)
    @grid[position - 1] = player
    @grid
  end

  def available_positions
    available = grid.each_index.select { |i| grid[i].zero? }
    available.map { |i| i + 1 }
  end

  def valid_position?(position)
    available_positions.include? position
  end

  def current_board
    [@dimension, @grid]
  end

  def board_array
    size = @dimension**2
    Array.new(size, 0)
  end
end
