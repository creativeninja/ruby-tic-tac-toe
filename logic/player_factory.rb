require_relative "player_human"
require_relative "player_computer"

# --------------
# Player generator generates the players and assign player number
# --------------
class PlayerFactory
  attr_reader :difficulty

  def initialize(difficulty = "E")
    @difficulty = difficulty
  end

  def create_players(whos_playing)
    # create the players and assign them the symbols they are
    if whos_playing == "H v H"
      create_human_v_human_players
    elsif whos_playing == "H v C"
      create_human_v_computer_players
    elsif whos_playing == "C v H"
      create_computer_v_human_players
    elsif whos_playing == "C v C"
      create_computer_v_computer_players
    end
  end

  def create_human_v_human_players
    {
      1 => PlayerHuman.new(1),
      2 => PlayerHuman.new(2)
    }
  end

  def create_human_v_computer_players
    {
      1 => PlayerHuman.new(1),
      2 => PlayerComputer.new(2, difficulty)
    }
  end

  def create_computer_v_human_players
    {
      1 => PlayerComputer.new(1, difficulty),
      2 => PlayerHuman.new(2)
    }
  end

  def create_computer_v_computer_players
    {
      1 => PlayerComputer.new(1, difficulty),
      2 => PlayerComputer.new(2, difficulty)
    }
  end
end
