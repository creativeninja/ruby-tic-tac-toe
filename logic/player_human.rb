# --------------
# Create human player
# --------------
class PlayerHuman
  attr_reader :player

  def initialize(player)
    @player = player
  end

  def make_move(board, position = nil)
    move = { position: position, player: player }
    update_board = board.place(move)
    { "board" => update_board, "position" => position, "player" => player }
  end
end
