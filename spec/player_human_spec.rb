require "./logic/board.rb"
require "./logic/player_human.rb"
RSpec.describe PlayerHuman do
  player1 = PlayerHuman.new(1)
  player2 = PlayerHuman.new(2)
  board = Board.new
  it "Human player 1 moves to position 3" do
    move = { "board" => [0, 0, 1, 0, 0, 0, 0, 0, 0], "position" => 3, "player" => 1 }
    expect(player1.make_move(board, 3)).to eq(move)
  end
  it "Human player 2 moves to position 5" do
    move = { "board" => [0, 0, 1, 0, 2, 0, 0, 0, 0], "position" => 5, "player" => 2 }
    expect(player2.make_move(board, 5)).to eq(move)
  end
end
