require "./logic/board.rb"
RSpec.describe Board do
  context "when dealing with the board itself" do
    it "it's empty" do
      board = Board.new
      grid = Array.new(9, 0)
      expect(board).to have_attributes(:dimension => 3)
      expect(board).to have_attributes(:grid => grid)
    end
    it "its size is 4" do
      board = Board.new(4)
      grid = Array.new(16, 0)
      expect(board).to have_attributes(:dimension => 4)
      expect(board).to have_attributes(:grid => grid)
    end
    it "pregenerate grid values" do
      grid = [1, 1, 1, 2, 8, 2, 1, 2, 0]
      board = Board.new(3, grid)
      expect(board).to have_attributes(:grid => grid)
    end
  end
  context "place a piece on the board" do
    board = Board.new
    it "player 1 place a piece in the 1st cell" do
      pp = { position: 1, player: 1 }
      expect(board.place(pp)).to eq([1, 0, 0, 0, 0, 0, 0, 0, 0])
    end
    it "player 2 place a piece in the 4th cell" do
      pp = { position: 4, player: 2 }
      expect(board.place(pp)).to eq([1, 0, 0, 2, 0, 0, 0, 0, 0])
    end
    it "player 1 place a piece in the 3rd cell" do
      pp = { position: 3, player: 1 }
      expect(board.place(pp)).to eq([1, 0, 1, 2, 0, 0, 0, 0, 0])
    end
  end
  context "show available positions" do
    board = Board.new
    it "is cell 9 available" do
      pp = { position: 9, player: 1 }
      board.place(pp)
      expect(board.available_positions).to eq([1, 2, 3, 4, 5, 6, 7, 8])
    end
    it "is cell 3 available" do
      pp = { position: 3, player: 2 }
      board.place(pp)
      expect(board.available_positions).to eq([1, 2, 4, 5, 6, 7, 8])
    end
    it "is cell 3 available" do
      pp = { position: 3, player: 1 }
      board.place(pp)
      expect(board.available_positions).to eq([1, 2, 4, 5, 6, 7, 8])
    end
  end
  context "check if position valid" do
    board = Board.new
    it "is cell 9 valid" do
      pp = { position: 9, player: 1 }
      board.place(pp)
      expect(board.valid_position?(9)).to eq(false)
    end
    it "is cell 3 valid" do
      pp = { position: 2, player: 2 }
      board.place(pp)
      expect(board.valid_position?(3)).to eq(true)
    end
    it "is cell 20 valid" do
      pp = { position: 3, player: 1 }
      board.place(pp)
      expect(board.valid_position?(20)).to eq(false)
    end
  end
end
