require "./logic/game_controller.rb"
require "./logic/board.rb"
require "./logic/player_factory.rb"
RSpec.describe GameController do
  context "determine who's turn" do
    it "player 2's turn" do
      board = Board.new(3, [1, 2, 1, 0, 0, 0, 0, 0, 0])
      players = PlayerFactory.new.create_players("H v H")
      game_controller = GameController.new(board, players)
      expect(game_controller.whos_turn?).to eq(2)
    end
    it "player 2's turn" do
      board = Board.new(3, [1, 2, 1, 2, 0, 0, 0, 0, 0])
      players = PlayerFactory.new.create_players("H v H")
      game_controller = GameController.new(board, players)
      expect(game_controller.whos_turn?).to eq(1)
    end
  end
  context "play game" do
    board = Board.new
    players = PlayerFactory.new.create_players("H v H")
    game_controller = GameController.new(board, players)
    it "play game" do
      expect(game_controller.play_game([1, 2, 3, 4, 7, 5, 6, 8])).to eq("end")
    end
  end
  context "run game" do
    it "player 2 wins" do
      board = Board.new
      players = PlayerFactory.new.create_players("H v H")
      game_controller = GameController.new(board, players)
      run_game = game_controller.run([1, 2, 3, 4, 7, 5, 6, 8])
      expect(run_game["result"]).to eq(2)
    end
    it "player 1 wins" do
      board = Board.new
      players = PlayerFactory.new.create_players("H v H")
      game_controller = GameController.new(board, players)
      run_game = game_controller.run([2, 1, 3, 4, 7, 5, 6, 8])
      expect(run_game["result"]).to eq(1)
    end
    it "it's a draw" do
      board = Board.new
      players = PlayerFactory.new.create_players("H v H")
      game_controller = GameController.new(board, players)
      run_game = game_controller.run([1, 2, 3, 4, 6, 9, 8, 7, 5])
      expect(run_game["result"]).to eq("draw")
    end
  end
end
