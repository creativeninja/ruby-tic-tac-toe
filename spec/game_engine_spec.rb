require "./logic/game_engine.rb"
require "./logic/board.rb"
RSpec.describe Board do
  context "given the board, determine" do
    board = Board.new(3, [1, 1, 1, 2, 0, 2, 1, 2, 0])
    engine = GameEngine.new(board)
    it "Did a player win?" do
      expect(engine.winner?).to eq(true)
    end
    it "Is it a draw?" do
      expect(engine.draw?).to eq(false)
    end
    it "Who won?" do
      expect(engine.winner_symbol).to eq(1)
    end
    it "all the same symbol for line 1" do
      line1 = engine.lines[0]
      expect(engine.all_same_symbols?(line1)).to eq(true)
    end
  end
  context "board arrays" do
    board = Board.new(3, [1, 1, 1, 2, 0, 2, 1, 2, 0])
    engine = GameEngine.new(board)
    rows = [[1, 1, 1], [2, 0, 2], [1, 2, 0]]
    columns = [[1, 2, 1], [1, 0, 2], [1, 2, 0]]
    diagonals = [[1, 0, 0], [1, 0, 1]]
    it "lines" do
      expect(engine.lines).to eq([rows, columns, diagonals].flatten(1))
    end
    it "rows" do
      expect(engine.rows).to eq(rows)
    end
    it "columns" do
      expect(engine.columns).to eq(columns)
    end
    it "diagonals" do
      expect(engine.diagonals).to eq(diagonals)
    end
  end
end
